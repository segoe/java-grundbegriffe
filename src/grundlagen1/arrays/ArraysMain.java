package grundlagen1.arrays;

public class ArraysMain {

	public static void main(String[] args) {

		String str0 = "old";
		String str1 = "mac";
		String str2 = "donald";

// Neuen Array erstellen und ausgeben:

		String[] strArrNew = new String[] { str0, str1, str2 };

		// Alternative Schreibweise cool
		String strArrNew2 [] = new String[] { str0, str1, str2 };


		System.out.println("System.out.println(strArray) --> " + strArrNew);

// Iterieren: mit der foreach Schleife kann man NICHT den Array f�llen?

		String[] strArrNewAlt = new String[3];
		
		for (String string : strArrNewAlt) {
			string = "filling";

		}
		for (String string : strArrNewAlt) {
			System.out.println("Erwartet: \" filling \" aber es gibt aus: " + string);
		}

		for (int i = 0; i < strArrNewAlt.length; i++) {
			strArrNewAlt[i] = "filling2";
		}
		for (String string : strArrNewAlt) {
			System.out.println("Erwartet \" filling2 \": " + string);
		}

// Hier erstellen wir einen Array aus Strings, aber geben die gr��e noch nicht
// ein, da wir sie viell noch berechnen wollen.
// Dabacg legen wir die Gr��e fest, und Printen mit einer foreach Schleife aus:
		String[] strArr;

		strArr = new String[3];
		strArr[0] = str0;
		strArr[1] = str1;
		strArr[2] = str2;

		for (String string : strArr) {
			System.out.println("Aus der Foreach-Schleife: " + string);
		}

	}
}
