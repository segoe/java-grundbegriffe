package grundlagen1.objekte;

public class Auto {

	// FELDER
	private int laenge;
	private double preis;
	private String farbe;

	// KONSTRUKTOREN
	public Auto() {
		System.out.println("Objekt erstellt");
	}

	// METHODEN
	public void beispielMethodeVoid() {
		System.out.println("i bin VOID (bayrischer akzent) und bin geizig, denn i geb nix �s");
	}

	// GETTERS AND SETTERS
	// click: 'Source -> Generate Getters and Setters...'
	public int getLaenge() {
		return laenge;
	}

	public void setLaenge(int laenge) {
		this.laenge = laenge;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public String getFarbe() {
		return farbe;
	}

	public void setFarbe(String farbe) {
		this.farbe = farbe;
	}

}
