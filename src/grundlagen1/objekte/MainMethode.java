package grundlagen1.objekte;

public class MainMethode {
	private static final String USERNAME_STRING = "username123";
	private static final int GLOBALE_ZAHL = 42;

	public static void main(String[] args) {
		
		// Instanzieren der Klasse Auto:
		Auto auto = new Auto();

		// Aufrufen der Methoden dieses Objekts:
		auto.beispielMethodeVoid();
		System.out.println(USERNAME_STRING + GLOBALE_ZAHL);
	}
}
