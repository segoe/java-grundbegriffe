package grundlagen1.staticAndNonStatic;

public class EineAndereKlasse {
	//Nicht-Statischer Teil
	private int zahl = 42;

	public int methode1(int num1) {
		System.out.println("Klassenvariable ist: " + zahl);
		return num1;
	}

	
	//Statischer Teil
	private static int staticZahl = 43;
	
	public static int methode2statisch (int num1) {
		System.out.println("Statische Klassenvariable ist: " + staticZahl);
		return num1;
	}

	public int getZahl() {
		return zahl;
	}

	public void setZahl(int zahl) {
		this.zahl = zahl;
	}

	public static int getStaticZahl() {
		return staticZahl;
	}

	public static void setStaticZahl(int staticZahl) {
		EineAndereKlasse.staticZahl = staticZahl;
	}

}
