package grundlagen1.staticAndNonStatic;

public class Main {

	public static void main(String[] args) {
		
		//Aufruf von Statischer Methode und Statischer Variable:
		// -> Keine Instanzierung notwendig, daf�r aber kein Zugriff auf die Nichtstatischen Felder (hier: private int zahl = 42)
		int i = EineAndereKlasse.methode2statisch(5);
		System.out.println(i);
		
		
		//Aufruf der nicht statischen Methode:
		// -> Zugriff nur �ber das erstellte Objekt
		EineAndereKlasse klasse = new EineAndereKlasse();

		i = klasse.methode1(65);
		System.out.println(i);
		
	}

}
