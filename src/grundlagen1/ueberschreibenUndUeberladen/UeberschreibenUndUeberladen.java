package grundlagen1.ueberschreibenUndUeberladen;

interface A {
    void v();
}

interface B extends A {
    void x(B b);
}

interface C {
    void v();
}


class E implements A, C {
    @Override
    public void v() {
        System.out.println("v() in E");
    }
    public void x(G g) {
        System.out.println("x(G g) in E");
    }
}

class F extends E implements B {

    @Override
    public void x(B b) {

    }
}

class F2 implements B {


    public void v() {

    }

    @Override
    public void x(B b) {

    }
}

class G {
}
