package grundlagen2.fehlerwerfen;



public class Box {
    String inhalt;

    public static void main(String[] args) {
        Box box = new Box();
        box.einlagern("abc");
        System.out.println(box.entnehmen());
    }

    public void einlagern(String inhalt) {
        if (this.inhalt != null)
            System.out.println("Fehler, die Box war nicht leer!");
        this.inhalt = inhalt;

    }

    public String entnehmen() {
        if (this.inhalt == null)
            System.out.println("Fehler, die Box ist leer!");
        String entnommen = inhalt;
        inhalt = null;
        return entnommen;
    }

}
