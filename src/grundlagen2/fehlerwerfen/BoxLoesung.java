package grundlagen2.fehlerwerfen;


class BoxIstVollException extends Exception {
};

class BoxIstLeerException extends Exception {
};

public class BoxLoesung {

    String inhalt;

    public static void main(String[] args) {
        BoxLoesung box = new BoxLoesung();

        try {
            box.einlagern("abc");box.einlagern("abc");
        } catch (BoxIstVollException e) {
            System.out.println("Fehler, die Box war nicht leer!");
        }

        try {
            System.out.println(box.entnehmen());
        } catch (BoxIstLeerException e) {
            System.out.println("Fehler, die Box ist leer!");

        }
    }

    public void einlagern(String inhalt) throws BoxIstVollException {
        if (this.inhalt != null)
            throw new BoxIstVollException();
        this.inhalt = inhalt;

    }

    public String entnehmen() throws BoxIstLeerException {
        if (this.inhalt == null)
            throw new BoxIstLeerException();
        String entnommen = inhalt;
        inhalt = null;
        return entnommen;
    }

}
