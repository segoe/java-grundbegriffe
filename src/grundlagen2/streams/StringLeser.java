package grundlagen2.streams;

import java.io.IOException;

public class StringLeser implements CharEingabeStrom {
	private String quelle;
	private int index;

	public StringLeser(String quelle) {
		this.quelle = quelle;
	}

	@Override
	public int read() throws IOException {
		System.out.println("\nindex: " + index);
		if (index == quelle.length())
			return -1;
		return quelle.charAt(index++);
	}
}
